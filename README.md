# Amazon Elastic File System Connector
Amazon Elastic File System (Amazon EFS) provides simple, scalable file storage for use with Amazon EC2 instances in the Amazon Web Services Cloud.

Documentation: https://docs.aws.amazon.com/efs/latest/ug/api-reference.html

Specification: https://github.com/APIs-guru/openapi-directory/blob/main/APIs/amazonaws.com/elasticfilesystem/2015-02-01/openapi.yaml

## Prerequisites

+ Generate an AWS Acess Key ID and AWS Secret Key from the AWS Admin Console

## Supported Operations
All endpoints are operational.
## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

